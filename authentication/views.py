from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,ListCreateAPIView,ListAPIView,RetrieveAPIView,RetrieveUpdateDestroyAPIView
from rest_framework import viewsets,status

from rest_framework.permissions import IsAuthenticated,IsAdminUser,IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication,SessionAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from authentication.pagination import ErpLimitOffestpagination,ErpPageNumberPagination

import base64


from authentication.serializers import (
    UserLoginSerializer,
    UserSerializer,


   )
from django_filters.rest_framework import filters
from rest_framework import filters
from django.contrib.auth.models import Permission

from django.db.models import Q

# Create your views here.dat

class CustomObtainAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        response=super(CustomObtainAuthToken,self).post(request,*args,**kwargs)
        token=Token.objects.get(key=response.data['token'])
        user=User.objects.get(id=token.user_id)
        serializer=UserLoginSerializer(user,many=True)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'username':user.username,
            'email': user.email,
            'is_superuser': user.is_superuser,


        })



class UserCreate(ListCreateAPIView):
    """
    Creates the user.
    """

    serializer_class = UserSerializer

    # def post(self, request, format='json'):
    #     user_serializer = UserSerializer(data=request.data)
    #     if user_serializer.is_valid():
    #         user=user_serializer.save()
    #         if user:
    #             serializer = UserSerializer(instance=user)
    #             return Response({'username':user.username,'message':'success'},status=status.HTTP_201_CREATED)
    #     return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

