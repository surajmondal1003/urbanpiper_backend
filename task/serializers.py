from django.contrib.auth.models import User,Group
from rest_framework import serializers
from task.models import Task,TaskStatus
from authentication.serializers import UserReadSerializer
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator

class TaskStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('id', 'status','created_at')



class TaskCreateSerializer(serializers.ModelSerializer):
    title = serializers.CharField(required=True)
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())


    class Meta:
        model = Task
        fields = ('id', 'title', 'priority', 'status','created_by')

    def create(self, validated_data):
        task=Task.objects.create(**validated_data)
        task_status=TaskStatus.objects.create(task=task,status=task.status)

        return task

    def update(self, instance, validated_data):

        instance.title=validated_data.get('title', instance.title)
        instance.priority=validated_data.get('priority', instance.priority)
        instance.status=validated_data.get('status', instance.status)

        instance.save()

        task_status=TaskStatus.objects.create(task=instance,status=instance.status)

        return instance

class TaskListSerializer(serializers.ModelSerializer):


    created_by = UserReadSerializer(read_only=True)
    deliver_person = UserReadSerializer(read_only=True)
    class Meta:
        model = Task
        fields = ('id', 'title', 'priority', 'status','created_by','created_at','deliver_person')



class TaskReadSerializer(serializers.ModelSerializer):

    created_by = UserReadSerializer(read_only=True)
    task_status_history=TaskStatusSerializer(many=True)
    deliver_person=UserReadSerializer(read_only=True)
    class Meta:
        model = Task
        fields = ('id', 'title', 'priority', 'status','created_by','created_at','deliver_person','task_status_history')


