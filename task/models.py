from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Task(models.Model):
    PRIORITY_CHOICES = (
        ('3', 'high'),
        ('2', 'medium'),
        ('1', 'low'),
    )
    STATUS_CHOICES = (
        ('new', 'new'),
        ('accepted', 'accepted'),
        ('completed', 'completed'),
        ('declined', 'declined'),
        ('cancelled', 'cancelled'),
    )

    title=models.CharField(max_length=200)
    priority = models.CharField(max_length=20, choices=PRIORITY_CHOICES, default='1')
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='new')
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True,related_name='created_by')
    deliver_person = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True,related_name='deliver_person')

    def __str__(self):
        return str(self.title)





class TaskStatus(models.Model):
    STATUS_CHOICES = (
        ('new', 'new'),
        ('accepted', 'accepted'),
        ('completed', 'completed'),
        ('declined', 'declined'),
        ('cancelled', 'cancelled'),
    )

    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='new')
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, blank=True, null=True,related_name='task_status_history')
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.task.title)