# Generated by Django 2.0.5 on 2019-07-03 18:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0002_auto_20190704_0000'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskstatus',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
