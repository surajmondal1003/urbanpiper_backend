from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,ListCreateAPIView,ListAPIView,RetrieveAPIView,RetrieveUpdateDestroyAPIView,RetrieveUpdateAPIView
from rest_framework import viewsets,status

from rest_framework.permissions import IsAuthenticated,IsAdminUser,IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication,SessionAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from authentication.pagination import ErpLimitOffestpagination,ErpPageNumberPagination

import base64


from task.serializers import (
   TaskCreateSerializer,TaskListSerializer,TaskReadSerializer
   )

from task.models import Task,TaskStatus
from django_filters.rest_framework import filters
from rest_framework import filters
from django.contrib.auth.models import Permission

from django.db.models import Q

# Create your views here.dat


class TaskCreate(ListCreateAPIView):
    """
    Creates the user.
    """
    queryset = Task.objects.all()
    serializer_class = TaskCreateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated,IsAdminUser]



class TaskList(ListAPIView):
    """
    Creates the user.
    """
    queryset = Task.objects.all().order_by('-created_at')
    serializer_class = TaskListSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated,IsAdminUser]

class TaskDetail(RetrieveAPIView):
    """
    Creates the user.
    """
    queryset = Task.objects.all()
    serializer_class = TaskReadSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated,IsAdminUser]


class TaskUpdate(RetrieveUpdateAPIView):
    """
    Creates the user.
    """
    queryset = Task.objects.all()
    serializer_class = TaskCreateSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]




##########Delivery person Views############


class TaskListDeliverPerson(ListAPIView):

    authentication_classes = [TokenAuthentication]
    serializer_class = TaskListSerializer

    def list(self, request, *args, **kwargs):
        queryset=Task.objects.filter(deliver_person=self.request.user)
        serializer=TaskListSerializer(queryset,many=True)
        return Response(serializer.data)


class NewTaskListDeliverPerson(ListAPIView):

    authentication_classes = [TokenAuthentication]
    serializer_class = TaskListSerializer

    def list(self, request, *args, **kwargs):
        queryset=Task.objects.filter(status='new').order_by('-priority','-created_at')
        serializer=TaskListSerializer(queryset,many=True)
        return Response(serializer.data[0])



class TaskActionDeliverPerson(RetrieveUpdateAPIView):

    authentication_classes = [TokenAuthentication]
    # serializer_class = TaskListSerializer

    def put(self, request, *args, **kwargs):

        task_id=self.request.data.get('task_id')
        input_status=self.request.data.get('status')
        task= Task.objects.get(pk=task_id)
        error=False
        if task:
            if input_status=='accepted':
                if task.status=='new':
                    task_count=Task.objects.filter(deliver_person=self.request.user,status='accepted').count()
                    print(task_count)
                    if(task_count==3):
                        error=True
                        message='Can not have more than 3 tasks in Accepted/pending state'
                    else:
                        task.status=input_status
                        task.deliver_person=self.request.user
                else:
                    error = True
                    message = 'Task is not in New State'

            if input_status=='declined':
                if task.status=='accepted' and task.status!='completed':
                    task.status=input_status
                else:
                    error = True
                    message = 'Task is not Accepted or it may have been Completed or It have been Cancelled'

            if input_status=='completed':
                if task.status=='accepted':
                    task.status=input_status
                else:
                    error = True
                    message = 'Task is not yet Accepted'

            if error== False:
                task.save()
                return Response({'message': 'success'}, status=status.HTTP_200_OK)
            else:
                return Response({'message': message}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({'message':'Task Not found'},status=status.HTTP_400_BAD_REQUEST)