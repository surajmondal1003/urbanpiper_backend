from django.contrib import admin
from django.urls import path
from rest_framework import routers
from task import views
from django.conf.urls import url, include




urlpatterns = [

    path('tasks', views.TaskCreate.as_view()),
    path('tasks_list', views.TaskList.as_view()),
    path('tasks/<pk>', views.TaskDetail.as_view()),
    path('tasks_update/<pk>', views.TaskUpdate.as_view()),
    path('new_tasks_list_deliver_person', views.NewTaskListDeliverPerson.as_view()),
    path('accepted_tasks_list_deliver_person', views.TaskListDeliverPerson.as_view()),
    path('tasks_action_deliver_person', views.TaskActionDeliverPerson.as_view()),


]
